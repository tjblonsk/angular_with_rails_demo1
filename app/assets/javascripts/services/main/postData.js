Blog.factory('postData',['$http', function($http){

  var postData = {posts: [{title:'loading', contents:''}], isLoaded: false};


  postData.loadPosts = function(){
    if(!postData.isLoaded){
      $http.get('./posts.json').success(function(data){
        postData.posts = data;
        postData.isLoaded = true;
        console.log('Successfully loaded posts.');
      }).error(function(){
        console.error('Failed to load posts.');
        });
      }
    };

  console.log(postData);

  return postData;

}]);