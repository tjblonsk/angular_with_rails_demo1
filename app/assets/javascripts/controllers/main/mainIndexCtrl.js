Blog.controller('IndexCtrl', function($scope, $location, $http, postData){
  // $scope.title = 'my blog';
  // $scope.posts = [{title: 'Loading posts...', contents: ''}];

  postData.loadPosts();

  $scope.posts = postData.posts;
  console.log($scope.posts);

  $scope.viewPost = function(postId){
    $location.url('/post/' + postId);
  };
});



